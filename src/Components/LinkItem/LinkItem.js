import React, {useState} from 'react';
import classNames from 'classnames'
import styles from "./index.module.scss";
import photo1 from '../../media/img/Rotshtein_Tashi_Cam_01_Night_Poster_Dark.jpg'

const LinkItem = ({title, text, color,subTitle,href, id,OpenItem}) => {

    return (
        <>
            <div className={styles.BlockLink}>
                <div className={styles.BlockLinkPhoto}>
                    {/*<img src={photo1} alt=""/>*/}
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/1999/xlink" width="50" height="47" viewBox="0 0 50 47">
                        <image id="firs_logo" data-name="firs logo" width="50" height="47" href={href}/>
                    </svg>
                </div>
                <div className={styles.BlockLinkWrapper}>
                    <div style={{background:color}}
                         className={classNames(styles.BlockLinkText)}>
                        <p>{title}<br/>{subTitle}</p>
                        <span className={styles.BlockLinkSub}>{text}</span>
                    </div>
                    <img onClick={()=>OpenItem()} src={photo1} height="94px" width='165px' alt=""/>
                </div>
            </div>
        </>
    );
};

export default LinkItem;