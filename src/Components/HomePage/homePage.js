import React, {useState} from 'react';
import videoMain from '../../media/video/רוטשטיין - אנימציית מרפסת.mp4'
import styles from './index.module.scss'
import LinkItem from "../LinkItem/LinkItem";
import classNames from 'classnames'


const HomePage = () => {

    const [activeDoor, setActiveDoor] = useState(false)

    const linkItemThree = [
        {
            title: 'להטבות', id: 0, subTitle: 'בנתניה >>', text: 'לצאת מהעיר, ולהישאר בה', color: '#036473',
            href: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD8AAAAXCAYAAAC8oJeEAAAACXBIWXMAAAsSAAALEgHS3X78AAACzUlEQVRYw+VYsY7bMAx9SbQemj9I/iD+g7g3dWv+oAHioVsNxEunOtMtGny7BuUHinzB1fkDt1/gbBl97dJBQDqUPvAI20lstJe0BARJtE2Jokg+ujdYLHMAMYAxgDkAC8DDb8oA+DSOAYQ0f0/NApgBCJ3ROf4A/fj44VCOb+7ue2y+vbm797vI7gPIqYH1CSkuKaNnewBwRlvijXGF1GdKWbJsAWBIrXxWUPOIX+AfIEUWLJhCmQqikA5jSNc8dUZnKogsvf80BpCyG3N1yoeS6YxO2DRl/A0blweRXrPlExVE5dwyv+Y8UDAsKSQXmLNg6LPgCGe0r4IoKYMnzdMamSHdplQYoTGgqSAaM1mFM3qmgshj+6/S50kvBWAqrJxX8CB4QwpyJc8H8KliAU98N62ROaxY4yg5o3M6gBEdhl+xTl4jN+0DWAtmDmArX6zgSfoKYNfxJq5ayIhrxlyfFbW1jPZWnib3c+bXTb6dOqO9roHPGR2fK4Niz45Z3JP6OKNjkm2rUt31pqsgkqk3PDfPXzNtAEzYfCRAWXOer/GTrUB9nFe0sNCmpXLbmnkmgttOKL7mqflk5cmPbBNPBdHsTCXeUv9I1jrpe4nfG/B8LixvL+nav6Y2po1y3FCINNqWeLSfnwRvWxLfcEIAZVpzVeXGxsxCOwI4Hllr0iFbWBVEMcl+R2OfHQQ/2LnqGGhiAK9ow5Mj+bYKaDyKjU0YP2m5L8sAV9wAckaqwymX6GrGStpUYH3bgA9SABkVVRyMAIA94//AqqIc53GgVk5vsFj6AL6UgggM/BekyHdlWnsJsFIWSh6lMQ/ANwCfAcyc0SErfB6ov2XvPgC4PVYMPVvTGc1/Vb0YsaBXFkQxgDcVEXwO4DsLXh6rKlv9ybk0sqTMT7qNG5G+9kdqjauHtx7bn8z9e5E2eX86HQ6Hi2mDxTL5m+v9Ake+0E4IfrhBAAAAAElFTkSuQmCC"
        },
        {
            title: 'להטבות', id: 1, subTitle: 'ברמת גן >>', text: 'קו ראשון לתל אביב', color: '#D1313D',
            href: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAvCAYAAAChd5n0AAAACXBIWXMAAAsSAAALEgHS3X78AAAIvElEQVRo3s1abWxT1xl+zv3yt3PtxHEClDqQkABhXMjKlIUpIZU2NH7MQpu0TZrwpgppP7a501ShjWnZxg86TcJs04SqSBhVox+q1FRiG6MDGY2q09og0w8GCzQOLdgxIbFDYhzb95792LnRwdghyXDglY507z0ffp/zfr8JoZRiqXSkpTsAoI+NLgA6m8qULQ0AiAOIAYg+P/puBjUiaYkAQgBCAJ4GcNthdzR43GrO56nPAEBTvQ8A4h0vHwpXAK1WAPrIiCxGIkdauoMAfgdg0m6z+zeu25BqUD1+gQj/Mm/cbplosisTsMpTKQBwHj0bwwrSgkCOtHSrAIYANKluVdnW0VmwKhYAgNt28z237YYOQONuOsapVICTwpDz6NlILYFIC4DQAJySZVn/3IbNkz7VS0VB99bZP0k6LOkGgN4hbsvflW3+IROMvPf4A8zO/OhZU7VWXiLMFg6q7jrXM5u3zUqiaHgcoyW7Ja0QGL+19K7NMVsxJRZ78Wa3/+fjm8a5YxL00M7EY1MtBuJXq/3NdEvrxoIsFdyN7g/yAiklxafcXilQRyCQf/w40f/GHyZbvgzgWQA2ABcA8IxrbAzQQzujKwqEqdObq/3NwpbWjQWLPAuf+2OVyLirbG8yiFUqXZh+Wvle4kvTiZLFCiD3jbZs/efXzCR71mfzZWcnegL7QyslEanMsP+iuuvELa0bp63y7IzP/ZGTuGQomt8xTW35H1zbNfHK9Cq1o66gHu9NZNY13FNFgSYB3D49Jv/pxoxw5cSefXEAeCfxkrqSXos39iFZlos7OrflCSEOt+3TQeKSf6JoflzO+4WvjOzWx3W54cVdNye+uC7bUjBI4fKkkPlwUlRnS6SRUhwAkNj35xPx/x1nifYEahc3KgJh0rB+Ycv2e6IgQpFmf2h1zxxTtGZczvux8+oeOC1U/9u3r9ocFt16MS3dvTghugyKnN1wKauMpmmHoVoJ0DAujl3JiOlULYPfQhJRG+t9DS67EwBONbo/eE7ZvkbPGrbUzqtfbdzkKwi/2TPq0kHnXh1RnNMFMuE0PK5VpVa7TJUMAIzP5q15XVfTOW/4k2nLMQDo+vBcfPhAf2YlgQx0ru8wANib1YvvSRu8uyEJ93Z9HHQ4LMj/evfYtA6UXvmPpU43JGtLcZPVbrjuzhZLrgup8dxEvuA2KE0DMAA4AYQB5AH4uw6fuwQgMnygv6aRXjrS0q36vA27LYqSAnBScho/E5uc9b9PPpP9aM6Rff3r1yVJ1D2vjSjQDQlthS6IVBL/mZwo3JjJCWKJeBon5TveSUmSi8JThOI2JdRniJjK2fVMqrHYPWvTO7oOn0sBCNZKQgKAkGEYLwEQm9XhtNTuLWR169RPU1tc32zPWlepc/KZG/JEXidTbYUu6CUBQ9c/84zdzRWaU4rRedmhNCcVwTInWAUD7xCK1wWDvC0VyR/dWemXbddsI4FPrf8GEAEQ7zp8TquVagXbA+tPAhiWLKXvCy7FdezWtvQcJcZz3UnbrRnBlswJt9cU220ilWb+OnYTeoE620ftBXtOKAC4ByA8eHJHNdUZMh+6Dp9LsIwgXAsgap3TvclumTgNQnwgxDZWcF4A0G+VjFTsltLpMOpSbqN+NYCrsyXd3T5qdzIQZwZP7lg0U8MH+uO1ADGvWgD6vI4RKy3obwPwDk6tnQQwOKeT9pkieTVQ7PyMgMinE8nBVePKWntOcAJ4fykgak3C86PvxpnP75NaPUlWXwTf+tZI0SLSs5vneiIAegGcms2VDjSmFReAycGTO8KEkBAhJAIAhJAYIURj30LsOcaezbkI9x5hQ+PmY1X2DhFCFswUhCvfOdjHmA+Iq1029qzWO4ptrL4IArh9PXP3TMOUbCcUSQAHubrDNN5elg3ztUgvO8OcM5PJXi6pNPdmuP383hiArzE+FlQts9aOcjV2iNUQMZbROt9PT23wTkoA0LCAYT9AlFI+I85wzJvzce5SMuV7KaUDAMbY/EOBwHn0bJQxnaGHdsYBBHsC+zMv7G2NAUgD0JQC8QF4o8pZu9glRAFEKaUxpnJLcbfV1CexGK81T/Le4/N1Q09g/wMdEULJnWo5lMl4hXm+aowzFfEwwCZlmTfbWrZ+SV5rsa5zueWqBuAEA5phzE6x2x/gAPYBeIuPO0uNI5mHie6Fva0miPBS6m9KKakgNVJhXfmZ962rMP8gkI6XD8XZjSyG4mXGGl3uDT6WvtaKMEJIoIKXWzxRSh86mDrRaoOtqfrOjXk3z9ao7L18XZTNLfi7bPRRSlcciDkSbE1sgTXRWgMZqLKmGpAYey+/ea0C45Hyc7jzBxbiQVopG6CUhlhw3Foh+GUppSFmKyiP/o+8G/8IqDxYjrHOfh0hJEopDVFKw8uNI0ulACGkr0JEr5p2sPV8gpmllMYIIWEAb7Jv+1iGG6KUZlbcay3CRioNjdsXYpLiPZtWgYcFbURYhkTGAJznxnIowl2k6Z0usU91AGJLTDaXBSRKKe0zxyLWn2epioddAgD0mgHQTOUppRqAExyYaK2BLNdrled0QUIIZWOAy+VM2voke637/gbEPYcJIfH/5w9CjxPINd79ct5rXiWfSNWqEEc0VmRdqmRXD6vRn8jslxm+afyJ5WTAT0wa/6i68Yu9Oa2sGaCxCN3HqU3CjMxsfaZCcyFT1jVJMFVLUEoTLMIHKKVxdnYC9//DgQpA5bOKJUmEEGK2h8yOyyiA77Koa9bgATNfYuvBtZVC3PsvmC3EuX4WmG1orPIMsjNjXL/MrFJ7+VJaWiTzURaB52+TSeI8Yy7BYkWEudFK7Zxq5XSANeBa2FkaV0abIMwzTMkFOP4iALSHei0WwfkoGyxPHMtuNMP9ABhovvln0iWuu3iirAHHq13Q7IQyEGb1OMb4C1NK+5ajWqEqDTWVSUljum0WSub3QJltmFLVOBsABw6cXfD7zG5lnM+S/wvYLMbFj2zkpQAAAABJRU5ErkJggg=="
        },
        {
            title: 'להטבות', id: 2, subTitle: 'בתל אביב >>', text: 'קלאסיקה תל אביבית', color: '#21BF73',
            href: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACYAAAA2CAYAAACr6+s1AAAACXBIWXMAAAsSAAALEgHS3X78AAAFHUlEQVRo3u2ZTWhcVRTHf4mDBotNMhSpGzPGXm1d9E1FV5Vm4qKCizYWXLixUxFEqHZq3TcB3RU7BIsETZkIVotFp4JUs9CX4MIGIe8VtdoLyVQUalraSWJKK8q46Ln1Zmbee/OGpJ1Fz2rueeeee+aej/c/57VVKhVWipRybGXnAQ9wgaLWfimOrrZVNKyaJoCC1n6h1QyzDcxp7XthQu3ceuoDppVyBlvtxmw6CWS19sutcGM27QRcpZyuFb0xpZyM1r5r1lNTbgXgwoU5zp2bZXx8ktnZ3xtRNaa1n10Rw5RySkAPsF9rPw9w7dqfNcrOnDnLyMhHTE5ORam8qSe2K5VyUnWuPRO2Z/PmTRw58hZDQ7ko9YeVclKxDVPKyQGzUjQBCsA8sFMpJx21f9euZzl58gO6u9eGiRWaDf7zQI9Sjgf8AHSKcWWAMwt/MHd9MXBzb++DFArvhJYSpZxMQzEmgimt/YLczLQ86gc8O9V73TcrAMlEBy8/8CTPr3+cNXfdXRvpY59y6ND7oYnQiGEukAIGgJJcd7k6i2zDDCUTHXz42Av03ruuRu+OHS+FZWx3e4RRacAE+zSQl4KYbcTvl/+5xos/f8zM1Us1z/bu3R22NRNqmNa+p7Wf1tpPAfujMjDIuNd//Zylf/9ext++vS8sETJRN1ZRyhlUyjFuTNd7fUTRzPUyX138qYa/desTQVvS7RFuBDgoLixIrDVFn138sYa3cePDQeJd7WFuBLol+wqAGwVVwmh66UIccSeqjqUk+LusJLgllIhw5bQUUIBOpZyuZmKsGYpyJVLdO6XqNx1jO5KPxhH3o1zZD2zR2m8DikCuWcP6k6qGNzXlB4mXo+qYC5Sk+u8DBsy7LA5tWbOeZ9ZtWsZbWroaBoXcRl7inuB0Q9vqIc4gSiY6OPzIQA3/xIkvw7Y1ZJgL+MAeibE3AK8RqJNZ28PXW17h/nvuq7mt0dHjQdvmtfbdRJRy+70ocMckQ05cjN0rJhMdPLW2h/6kqnGfoeHho1y5shB0ZDEWtFbKyUucITf4GjAp6+e09ov1oHU1jY9PcODA22EiD2ntl+IAxYIYNA9kgUULPDbU/jdg1JgZJcRqRiTo01r7rrgxLWAxE9SMmJgaHj7KsWNfhKmfF0BaDq38AfFWlmQwN5i3eoAampu7xKlT3zI6ejwspgwta3xXtBMfGXm3ArC4uMTp09N43tlGtw5p7Q+20oigbrPbCiOCoSCYnrhNBs1LTBVbaQw1JtlXbAqPrcINFYB8oyPPxCoZ4Ul37gkkd+MqWdGsvCUI9o5hdwy7Y9htorYNGza7VY2HJ0DQwJk08A3wtCVXvc5p7XsGalswKaOUkzX6tPZzddZ5OeM94FWpf9l2wVdmOJcWNGp+l6VDWrBwWB9wzvpd5sasPmXpwuqsSsLLSM9Qkj+9T76OmDPajByQb5dpjmvgseklrcZgQqr3oDlUa/8TeT4hYLFT/sxBq7O62ZvK3iLgyDone3PWGb+IXB7YnQhoOgxk9kKGdWkLxcbBbFkxKAVkzBlKOWkJhT5gTwIY4v/ZqqF8QIPhVsm44oqiyA9VydTbWxL56rGWge15rf1iy74rEzHdkAEuV5WaLgnmLmtt882MzYszwooLewaB74EOE4fCH5DDy8I3LvsN2GZcZD1b8QLrAt/JITmrpHhiXFrix5SFGclMUxZWtfL/pbVfksAtACWZXeS58c3b9Jqe8AtWH9ow/Qf67Fm0fzP+fAAAAABJRU5ErkJggg=="
        }
    ]
    const linkItemForce = [
        {
            title: 'להטבות', id: 3, subTitle: 'בבת ים >>', text: 'לחיות יוקרה בין תל אביב לים', color: '#CB0671',
            href: ""
        },
        {
            title: 'להטבות', id: 4, subTitle: 'ברמת השרו >>', text: 'יצירת מגורים מעוררת השראה', color: '#524641',
            href: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD4AAAAmCAYAAACcRCiyAAAACXBIWXMAAAsSAAALEgHS3X78AAADj0lEQVRo3u2ZMWsbMRTHfyqBQulg9wvUOLSzIV8gGAdnNBzx7KlZvSWj6VRDh6w1FAwtHeIeZLTJYW7sUEOmQgs5UujQKedvoC5PIB/nxD6f7KTJA6GT9fTX+0vvPZ3OSmvNQ5QnPFB5JP5I/JH4/y1bAN5+bUfatRSdAIiAN1LHQBHAHwYDo5TAmNEDykDkD4OBt187kLbpD2RMzx8G8VqJy8SxGPRGjClL/Q44BHaAI2BikTFETNvIqeDF1iIcAQPBN3MOEgve3YSrF6UcWyQGFqmJtYO2TKzFwHqO5DkwY739WtFaUCy8YFMx3jNE/WHQBZrAnj8MAuBQ6h6wLX2m4A+DyB8GkSEgutvAnrhv14wVV25K+4Uscs8qaxOltcarV4+AHX80btqdXr16mXBhxO0j4HwO5qE/GvdkvBNcC2PPH40jad/2Ctr1R+PjZIxHaYnNH423EwafArE/GgeASup79WoRuPTq1YE/GscOcVPFH41V2u9evVoGvsv4ie3q8Zz4TUpRdOdNHCewXOEuJeIVMzkqC/FoiTld4eaW3OKUmJtnIEsSd4GbD3GTIBaQ8hK6znDzPsdjSQLMSRA3xuFNu+4IN22e2rJvbovEY2bijnBJOQ7PvXo19fi7iXi0gIFZ3NEVbjKsBmlH4UKuvoCBONrxjV5LbzOwnHFnXOHmRjxyFIvRmmJ8pQ8Rrgy808QD4OAG3YOM10dXuPkQl5f3yKtXT+Vstc/aD0BkXvCXzLZOcJcRmbdse9ZWQqcpX1yurfPQ3NebK8ztCtcmt8i1dDJzH3d+6VeqBOwCBWAKXGmtQ+kraK2n8tzSWvdFv6K1PnP9zS0LmQJQEUJToKC17iR0GkALOAMaQF9qgFApVQF2lVJ9+W1XKXUBdICpUsos0pXrrL4QYTG0bYwFLqx2UlpST4VQCWjLrobSV5LxJ8CV6IbAtei2lVJ9pVRFFjyfT08Z3LaktQ5ldy6EfMu4b8qYUNy8I4ROgI+AJyq/5PvbNTAEnkspAM+AV8BvM9aExlqJW2RaslMmfkPgJ/DUNk70prKThnhbiHwC3gOfgR/Aa+AP8NXCnALkQXZGtNaZihjVEBIt2cWplLal17GeW8AX4GXWefMqWUmXxMUbkuBC4K2J9QXGdiRrb4z4Kq5uElEF+At8kzhvcA9k5XNckp05xtq5x+JdJW6d1+F9Ib22N7e7KA/2//F/YyvLlTSIhf4AAAAASUVORK5CYII="
        },
        {
            title: 'להטבות', id: 5, subTitle: 'בבאר יעקב >>', text: 'הזדמנות נדירה לקוטג בגוש דן', color: '#FCB038',
            href: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADkAAAAvCAYAAABZl2IDAAAACXBIWXMAAAsSAAALEgHS3X78AAAHR0lEQVRo3u1Zb2wUxxX/zd6sQwwoCxgpQIKXoEqVlfTWsWNsuY7XaUnkNLEvshMJlIQrtBVCkTiaKmmlVmw+VA0lDYdESURLdGmrhA92OZtgF5ngMxShEEdZVy0qH1IWmoSUJvQcHJd6/7x+8CzZO87ng9rXo82TVjez++bNvHn/fjPHiAjFoOGuVkWW5aRcJjfbtv1IuK03iSKRhOJRDEAawDcBxIs4b1GVjAIwqlq7EwCU4a4H9RtSyf3btXwLrwy395qinQRQNCX5TAnq3hbWJYkNHthRM8rlUJLzkLF64wlrCvaM96lX7lUcx426jhNxbBeO48JxKNbxrGmWqruqQglzcG+TEfww0tPmW08X8Ynjv27RBL8uYjUKoDl7I/4TYjOZXfdv14jz0IqHt7xjDbzcoHKZJ7nMk01PDhrDXa0JWZZVuUxO2ba91Z5wqh3bhus4Kdt2Y/r6owkAOLiz1nAcV2t/+t1IqSaenQASACBcNQJg64nXVqsADAAaAAXAI7WdfaawXNxX8NDuVZrIwsZMLorNdJ08sKMmweWQynkounrjCevYr1qSnPNUw9qB+EhPmyYsmbQnnJRj20b9mgEVAA7vaYy6jhN3bDf2jc3DiZJWEgD6d9XFOA8ZXOYpLvM051xrWDugAcCp/g7Ftm3DnnCiwl1N23Y1x3FN13GMBza9lZrp9bDZQjwDLzcoXOYRLnOdc75OLuOjsiybcpls2rZt2RNOxLHtlIhJU19/NH2jggHLj1GRdQ3xThXxuVW8ixze06jcMEoe2FGj9O+qSwD4h1AgCmAUgCnayXBbb0wknVcDZSN1aPcqpeSV3L9dUwD4MbWgZcMxvy4majv7VVEbzZHetohQeh2AlL7+qF8jrYM7a7WSRTy+MgDM1qdORgFgcG+TxmW+DsCK4a7WuCzLm4X1EsJlR/yS8fXvHE8ISybFt9KzZPe2sCZQS0wkHk0seEvD2gE/Dp+rau2OCotGhJvGUq/cawDAA5veigOo7PlZtVaq7hoVrqr176qLi3a86clB/1jVHjhiJQDo9WsGTJGA9MN7Gq1Du1fFhHWVUnXXtFic4SvbsuFYBv4Mt/emszAuGp8YtISSutiotI9rS75O5rgZSMuyrIfbe82R3raEPeGYtZ19RTk8F/PQnARgnOrvUEQ8Joo1MS+ikjH/CAYgVtvZly7WxEVzV3Ge1OQyWa9q7S7qHU9RlfxvkYT/A/pCyf8VYijsatAUxVsRbQ3AIIAhUQ60aW7lVAAWEVmMMTUHNk0LXi0ABgrFr1YhvFTAowsU47cjop0S/anGGeIhAIZIckYOvqCc1BQ8NMW4aXmDdXIoxwZoAG7JeqcELppMsevZY1UAlXl2figgKzwND/LINkR/Kt7mDEsSEbKfLOv5bR98J3ONybLWVZbM4tNzWXIquWKMKfgS+fgE71WWLBieEVHsSlAzpuSISXVWEghjUWH5Ud+bGGO6nx+ISJ8tWOcnoeshJY9Ck6YgYoHN9MMkTkTWbGBXM5AJc9FogCdfTGbj2WAWno63UsxzTbCw4DpJRDEi0oloqj9hTPFdL+SEIdysOZBA8vEqgQ2JEdE1gXueNWnB7nQdpGbN4Sv2aqB+TkVxkeXPEtE1H9GC7jqI2aV14smlQD4rqoFx0es9Tw4VeLWR79tQIB6Ddc7KU4MhUJCZR46v2JDgTRU4f0btL9mjVnZ2/QKgT7dhXc9/JcUYA2MAkxgkiYFB/EoMjIk2kyb7koQQAyBJkFiQR4IkSUKOhJAkCXnS533GIIUkeJ4H8ggeiV/Pg+cRiDx4rgciwqdjtkLkoXxOKE1EIM+DRwA8Dy4BJGQQTY4FETwPIPpcLtFknwfxXanQ+GUX88s5PF+BYl5kHTcv4sjJj8EAfPD3y9j7XA3OnR/HL7stgAEMDKm3L+DAS804mPoQKyvn42v1S/D6G2fweNtKbPnJSez8YX2GzO/99J1JnElAdZWCR++/Hc+8YGLbd8PwyMP3X/wDfrz5Tgz/8SIGTlwAiPCnv1zC68/XzI6SjdpCfPXuCoQY8NsjHwEAli8px7c7V0BdNg+/+/3fUF21AIsX3IQ1D1bi6e3voiG8OK/MF56pueKu+/rOAADOnh/Hsy+OoKmmAk01FSifE0LT3YvQWL0I8DxsfenPxU88v+g6g/HLLvb1n8VDzUsBAOU3czykL0PPkfevWd6dX7oFnfffjt+8cQ4dq28r7vXHJ+kJnLbGcNoaw4WL/8LH6Ykr3/qOnseGjjtQPic0aY0PP8Mdt81Dz5t/xdi4DQDY8YO6gudqrl2MyH1L8fN97wEAzn30T5w+cwmnrTFc+szBJ4G5Z9Rdg9RyT0VGv+6uhVCXzbuK70eb7sK8uWUFybyv/lYAwMbHVk6igIiKU+99ehXftzrU2Us8i5QyVCyck1FCfFq+pDyDt3Lp3IwSUggtXnATPNdD5dK5IJFVv7xiPogIy2+9OWcJKYT+DUqvivxzAzvbAAAAAElFTkSuQmCC"
        },
        {
            title: 'להטבות', id: 6, subTitle: 'בטירת כרמל >>', text: 'הדור הבא של מתחמי המגורים', color: '#FC5E00',
            href: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAAbCAYAAADBPvmtAAAACXBIWXMAAAsSAAALEgHS3X78AAAEDUlEQVRYw91Z0Y3bOBB99Po/7mB93xFwOqSA1VYQYVWA3UHUgbUdKBVELoAHdRClgOC8AAvQdmD/Z8H7yOPeeEJZ3sXZTjKAYImkRuSbN8MZ2njvcWoxSVGJx4y/DYCcvynvSwBLAAsA9wBmAFq2b/lcemf7U895gguJd7YBsAEwJyhbgtaxvyJgGYCNd3YJoCZw+J2A2ZAdJVk0IyABnFo8h/45gP4ShjsnMFvvbO+dDQC1AFrvbEcW5ABq9n/kmIoM6qijF/endf8zxpiOIPwSYvD2LlO+H53808LM6PPPDLhae0n7VLFjM6BHfq+/Wvs+AuScbnRwTiYpsoH41fE7/fTr3YxB+/mbseBtkiKV46ak7LUY8MdA1M8BfBLP9yEeRPoe5cKUfFY6qsiYJYDVngHHdUGN/0z9vZrbWgdwGuIf0fRlQl/Xk4pJB+BWXLXoK9XYa5MU+YAeqaM50+73KJoWBEKKNk415QI/KGB+sCIpP0TBPwesrkHH9Ovd/sTPEzIqxZoqEIAgLSRbvLPdhG7zoKydal82SVGJa36ALUHeM/bE6B+u/zMn+eKdNd5ZQ0Oaq7WvBliTi7lVMfaE7boecaeaPr8SWaiMPUF2R7rlJUQC8AZASXAkW9Yh0AdgBuMM2SFdpfXObtm35Ecke3ZHsOkUkpqk6OQ1EmvKyPyqvQSPC11LREXw1C83A4zYhaTtkFueUN4AuFHXGGtWii19LPNtI9uzdpXHQDUy6SbCJO2W5c/iSxHWDO5ME/FSGwlQmcxxDrDlGVgmdkOB7pTyoFKB22MAiLElJHh6cR8E1Zoj3eiRwA7pyc+Qs2yPKTm8sw1LlOtDYE0iu89eoqa2w55ulKu+dkTPT+NOohiVYP2Qn031AJMUDwMJ2yE3aiJ6bo+Y4MokxUq13UeSSE+95mlhKgCrq7XXZcJNGKeMmQ2s5SC7ppG2WmWJALBj4AoF43vp27GC8du7vzeysHxamDQUnZcWzq1/KTBtBJj2WLYImakib6eq3EtKrXZUMwqMd3ZrkuIvtYhNpJiM9e3VVk8Ls6fn+9HF3e2R/j9EdekGY7q2A+3lmJHOclD1K8qLjzYjJftvKdNXvFOapGj1iVlI/b2zm3CyJrLkTFOb41J1srY9cJLXk/6zcAqn9IZ35fhwv9G6RwnwUlfiYjJecwbmOX0/E5PK8f2ctzZJ0YszkIq71UyMLwFU3tk88q2ScSzjOzX++5ulFno7trfUH3aeYLDqpK7ErTkA0fKDc+9sx/uMW3ut2NAEFnln5eKCzlmo2Fkdp2zvOX5Gy7di8c961WbQsD/FK/9VeO3fJ1koAUxSNBFWBWsGF0sHQK60Du9s453NRG60DEcI3lmZQQ/qVS71KvkX1jz9OKab6uoAAAAASUVORK5CYII="
        }
    ]

    return (
        <div className={styles.wrapperMain}>
            <div className={styles.headerWrapper}>
                <video src={videoMain} loop muted autoPlay>
                </video>
                <div className={styles.headerWrapperTitle}>
                    פטור ממדד לשנה / הטבה בשינויי דיירים! / ליווי אדריכלי לעיצוב / פטור משכ"ט עו"ד / תנאי מימון 20/80 /
                    פטור ממדד לשנה! / ליווי אדריכלי לעיצוב הדירה / הטבה בשינויי דיירים! / פטור משכ"ט עו"ד / תנאי מימון
                    20/80
                </div>
            </div>
            <div className={styles.blockInfo}>
                <div className={styles.blockInfoForm}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="725.707" height="97.937"
                         viewBox="0 0 725.707 97.937">
                        <path id="Контур_141" data-name="Контур 141"
                              d="M642.172,0V27.618h54.91V97.937h28.624V0ZM600.31,97.937h29.208V0H600.31Zm-122.672,0,110.8-.2V37.8c0-23.113-13.826-37.8-37.971-37.8H523.98V27.814h22.782C557.472,27.814,560,33.1,560,44.659V69.73H506.066V0L477.637.2ZM435.385,0V51.318c0,13.32-6.815,18.608-16.746,18.608h-8.178V0H381.448V69.927H357.692V0H328.874V97.937H421.95c22.587,0,41.67-16.65,41.67-40.742V0Z"
                              fill="#fff"/>
                        <path id="Контур_142" data-name="Контур 142"
                              d="M206.59,97.937l110.8-.2V37.8c0-23.113-13.826-37.8-37.971-37.8H252.933V27.814h22.783c10.709,0,13.241,5.289,13.241,16.846V69.731H235.019V0L206.59.2ZM163.949,57.2h29.987V0H163.949Zm-42.058,0h29.987V0H121.891ZM28.818,69.731V27.814H68.152c10.32,0,13.241,3.526,13.241,14.691V69.731ZM0,97.937l110.212-.2V36.824C110.212,13.711,95.8,0,72.046,0H0Z"
                              fill="#e50d3f"/>
                    </svg>
                    <p className={styles.subTitle}>
                        <span>הטבות סופשנה</span>
                        <span className={styles.fontPink}>חוזרות ביג טיים</span>
                        <span className={styles.fontPink}>!</span>
                    </p>
                    <p className={styles.subTitleText}>
                        רוטשטיין עם הטבות גדולות מתמיד בכל הפרויקטים שלנו ברחבי הארץ
                    </p>
                    <form>
                        <p className={styles.formTitle}>
                            רוצה לשמוע עוד? מלא פרטים ונחזור אלייך בהקדם
                        </p>
                        <div className={styles.formInput}>
                            <input type="text"/>
                            <input type="text"/>
                            <input type="text"/>
                            <input type="text"/>
                        </div>
                        <div className={styles.buttonWrapper}>
                            <svg className={styles.checkboxSvg} xmlns="http://www.w3.org/2000/svg"
                                 xmlns="http://www.w3.org/2000/svg" width="196.721" height="41.151"
                                 viewBox="0 0 196.721 41.151">
                                <defs>
                                    <clipPath id="clip-path">
                                        <rect id="Прямоугольник_70" data-name="Прямоугольник 70" width="196.721"
                                              height="41.151" transform="translate(0 0)" fill="none"/>
                                    </clipPath>
                                </defs>
                                <g id="ROTSHTEIN_LOGO" data-name="ROTSHTEIN LOGO" transform="translate(0 0)">
                                    <g id="Сгруппировать_95" data-name="Сгруппировать 95">
                                        <g id="Сгруппировать_94" data-name="Сгруппировать 94"
                                           clip-path="url(#clip-path)">
                                            <path id="Контур_144" data-name="Контур 144"
                                                  d="M194.761,8.388a8.228,8.228,0,1,0-8.229,8.28,8.253,8.253,0,0,0,8.229-8.28m-8.229,16.767-9.988,15.152h19.976ZM157.993,40.307h12.83V.109h-12.83Zm-16.681-16.49H147.2V.5h-18.73V6.139h12.841ZM128.849,36.629l-2.88-4.163H124.9l5.376,7.6h-5.241v.825h6.163V40l-1.773-2.562,2.745-4.953-1.021-.017Zm-9.923-12.813h5.981V.5h-5.981Zm-2.061,17.073h.886V32.466h-.886ZM90.411,23.816l24.942-.048V8.986c0-5.226-3.292-8.488-8.668-8.488H100.8V6.186h4.914c2.826,0,3.8,1.075,3.8,3.871V18.08H96.3V.5L90.411.542Zm.49,8.65h-.886v8.424H90.9ZM86.75,14.395V.5H80.863V13.37c0,3.265-1.852,4.758-4.636,4.758H75.115V.5H69.133v17.63H63.617V.5H57.639V23.816H77.108c5.379,0,9.643-3.73,9.643-9.422M61.194,32.466H55.163v.824H60.29v6.775H55.18v.825h6.933v-.825h-.919Zm-32.159-8.65,24.94-.048V8.986C53.975,3.76,50.685.5,45.306.5H39.419V6.186h4.914c2.829,0,3.8,1.075,3.8,3.871V18.08H34.924V.5L29.035.542Zm-9.829-9.84h6.166V.5H19.206Zm-9.643,0h6.166V.5H9.563ZM6.439,40.889h.886V32.466H.846v.824H6.439ZM.109,27.861H5.95V.5H.109ZM1.817,35.77H.931v5.123h.886Zm13.655-3.3h-.9V37h.9Zm8.164,0h-.9V37h.9Zm8.133,7.617H30.6v.807h1.174a2.387,2.387,0,0,0,2.461-2.627v-5.8H30.815v.792h2.543v5.072a1.579,1.579,0,0,1-1.59,1.752m15.48-7.617H41.222v.824h5.124v6.775H41.239v.825h6.932v-.825h-.922ZM75.218,40.2v.842h.117a2.134,2.134,0,0,0,2.295-2.409V33.29h4.3v7.6H82.8V32.466H75.419v.824h1.323v5.359c0,1.063-.471,1.534-1.524,1.552m23.9-.118h-1.17v.807h1.17a2.388,2.388,0,0,0,2.462-2.627v-5.8H98.16v.792h2.546v5.072a1.581,1.581,0,0,1-1.593,1.752m10.459-7.617h-.9V37h.9Zm34.257,0h-5.11v.81h1.024v7.614h.886V33.276h3.216a1.69,1.69,0,0,1,1.876,1.852V40.1h-3.485v.792h4.337V35.213a2.49,2.49,0,0,0-2.745-2.747"
                                                  fill="#fff"/>
                                            <path id="Контур_145" data-name="Контур 145"
                                                  d="M194.761,8.388a8.228,8.228,0,1,0-8.229,8.28,8.253,8.253,0,0,0,8.229-8.28m-8.229,16.767-9.988,15.152h19.976ZM157.993,40.307h12.83V.109h-12.83Zm-16.681-16.49H147.2V.5h-18.73V6.139h12.841ZM128.849,36.629l-2.88-4.163H124.9l5.376,7.6h-5.241v.825h6.163V40l-1.773-2.562,2.745-4.953-1.021-.017Zm-9.923-12.813h5.981V.5h-5.981Zm-2.061,17.073h.886V32.466h-.886ZM90.411,23.816l24.942-.048V8.986c0-5.226-3.292-8.488-8.668-8.488H100.8V6.186h4.914c2.826,0,3.8,1.075,3.8,3.871V18.08H96.3V.5L90.411.542Zm.49,8.65h-.886v8.424H90.9ZM86.75,14.395V.5H80.863V13.37c0,3.265-1.852,4.758-4.636,4.758H75.115V.5H69.133v17.63H63.617V.5H57.639V23.816H77.108c5.379,0,9.643-3.73,9.643-9.422M61.194,32.466H55.163v.824H60.29v6.775H55.18v.825h6.933v-.825h-.919Zm-32.159-8.65,24.94-.048V8.986C53.975,3.76,50.685.5,45.306.5H39.419V6.186h4.914c2.829,0,3.8,1.075,3.8,3.871V18.08H34.924V.5L29.035.542Zm-9.829-9.84h6.166V.5H19.206Zm-9.643,0h6.166V.5H9.563ZM6.439,40.889h.886V32.466H.846v.824H6.439ZM.109,27.861H5.95V.5H.109ZM1.817,35.77H.931v5.123h.886Zm13.655-3.3h-.9V37h.9Zm8.164,0h-.9V37h.9Zm8.133,7.617H30.6v.807h1.174a2.387,2.387,0,0,0,2.461-2.627v-5.8H30.815v.792h2.543v5.072a1.579,1.579,0,0,1-1.59,1.752m15.48-7.617H41.222v.824h5.124v6.775H41.239v.825h6.932v-.825h-.922ZM75.218,40.2v.842h.117a2.134,2.134,0,0,0,2.295-2.409V33.29h4.3v7.6H82.8V32.466H75.419v.824h1.323v5.359c0,1.063-.471,1.534-1.524,1.552m23.9-.118h-1.17v.807h1.17a2.388,2.388,0,0,0,2.462-2.627v-5.8H98.16v.792h2.546v5.072a1.581,1.581,0,0,1-1.593,1.752m10.459-7.617h-.9V37h.9Zm34.257,0h-5.11v.81h1.024v7.614h.886V33.276h3.216a1.69,1.69,0,0,1,1.876,1.852V40.1h-3.485v.792h4.337V35.213a2.49,2.49,0,0,0-2.745-2.747"
                                                  fill="none" stroke="#f5f6f7" stroke-width="0.217"/>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                            <div>
                                <input type="checkbox"/>
                                <span className={styles.checkboxTitle}>אני מאשר קבלת מידע שיווקי</span>
                            </div>
                            <button className={styles.checkboxButton}>
                                שלח
                            </button>
                        </div>
                        <div className={styles.checkboxCreate}>
                            <p>ההטבות לחוזים שיחתמו עד ה- 31.12.21 בלבד!</p>
                        </div>
                    </form>
                </div>
                <div className={styles.wrapperLinks}>
                    <div className={styles.wrapperLinksFirst}>
                        {linkItemThree.map(({title, subTitle, text, color, href, id}) => {
                            return (
                                <LinkItem OpenItem={() => setActiveDoor(true)} key={id} id={id} title={title}
                                          subTitle={subTitle} text={text} color={color} href={href}/>
                            )
                        })}
                    </div>
                    <div className={styles.wrapperLinksFirst}>
                        {linkItemForce.map(({title, subTitle, text, color, href, id}) => {
                            return (
                                <LinkItem OpenItem={() => setActiveDoor(true)} key={id} id={id} title={title}
                                          subTitle={subTitle} text={text} color={color} href={href}/>
                            )
                        })}
                    </div>
                </div>
            </div>
            <div
                className={classNames(styles.blockDoorOne, {
                    [styles.active]: activeDoor
                })}>
                <div onClick={() => setActiveDoor(false)} className={styles.close}>
                    <p>חזור<br/>
                        לדף הבית</p>
                </div>
                <div className={styles.blockDoorOneContent}>
                    <p className={styles.ContentTitle}><span className={styles.fontPink}>weqwe</span>werwer</p>
                    <p className={styles.ContentTitle}>בת ים</p>
                    <div className={styles.CountBlock}>
                        <p>
                            פטור ממדד לשנה + תנאי מימון 20/80 + <br/> פטור משכ”ט עו”ד+ הטבה בשינויי דיירים
                        </p>
                    </div>
                    <div className={styles.blockDes}>
                        <p className={styles.blockDesTitle}>
                            בואו לחיות בין תל אביב לים - Blue & The City
                        </p>
                        <p className={styles.blockDesText}>
                            מול חוף הסלע היפהפה של בת – ים ואופק כחול אינסופי, רוטשטיין גאה לבנות את Blue &The City.
                            מגדל יוקרה עם כל הפינוקים, עם נוף פתוח לים מרוב הדירות ועם גישה מהירה ונוחה לכל מה שאתם
                            אוהבים בתל אביב, מתחנת הרכבת הקלה שבסמוך.
                        </p>
                        <p className={styles.blockDesSubText}>
                            אל תיקחו את הזמן, קחו החלטה.
                        </p>
                        <p className={styles.blockDesTitle}>
                            4-5 חד׳ החל מ-
                        </p>
                        <p className={styles.blockDesCount}>
                            ₪2,770,000
                        </p>
                        <span className={styles.blockDesBotTest}>
                            קומה 11 (רואה ים)
                        </span>
                    </div>
                </div>
                <form className={styles.blockForm}>
                    <p className={styles.blockFormTitle}>
                        רוצה לשמוע עוד? מלא פרטים ונחזור אלייך בהקדם
                    </p>
                    <div className={styles.wrapperInput}>
                        <input className={styles.blockFormInput} type="text"/>
                        <input className={styles.blockFormInput} type="text"/>
                    </div>
                    <div className={styles.wrapperButton}>
                        <input className={styles.blockFormInput} type="text"/>
                        <div className={styles.wrapperButtonFix}>
                            <div>
                                <input type="checkbox"/>
                                <span className={styles.checkboxTitle}>אני מאשר קבלת מידע שיווקי</span>
                            </div>
                            <button className={styles.blockFormButton}>שלח</button>
                        </div>
                    </div>
                </form>
            </div>
            <div
                className={classNames(styles.blockDoorSecond, {
                    [styles.active]: activeDoor
                })}>

            </div>
        </div>
    );
};

export default HomePage;

